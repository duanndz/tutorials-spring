package com.duanndz.spring.application;

import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Created By duan.nguyen at 12/21/20 11:29 AM
 */
@SpringBootApplication
public class BasicApplication {

    @Bean
    public ExitCodeGenerator exitCodeGenerator() {
        return () -> 42;
    }


    public static void main(String[] args) {
        System.exit(
            SpringApplication.exit(
                SpringApplication.run(BasicApplication.class, args)
            )
        );
    }

}
