package com.duanndz.spring.application;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Created By duan.nguyen at 12/21/20 1:39 PM
 */
@Component
@Slf4j
public class BasicCommandLineRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        log.info("Arguments {}", (Object) args);
    }
}
