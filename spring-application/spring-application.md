# Spring Application

## 4.1.10. Using the ApplicationRunner or CommandLineRunner
    If you need to run some specific code once the SpringApplication has started, 
        you can implement the ApplicationRunner or CommandLineRunner interfaces. 
    Both interfaces work in the same way and offer a single run method, 
        which is called just before SpringApplication.run(…args) completes.

- CommandLineRunner

    The CommandLineRunner interfaces provides access to application arguments as a string array, 
        whereas the ApplicationRunner uses the ApplicationArguments interface discussed earlier. 
    The following example shows a CommandLineRunner with a run method:
  
## 4.1.11. Application Exit
    
    Each SpringApplication registers a shutdown hook with the JVM 
        to ensure that the ApplicationContext closes gracefully on exit. 
    All the standard Spring lifecycle callbacks (such as the DisposableBean interface 
        or the @PreDestroy annotation) can be used.
    


