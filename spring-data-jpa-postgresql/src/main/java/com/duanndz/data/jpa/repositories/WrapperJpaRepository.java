package com.duanndz.data.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Created By duan.nguyen at 12/22/20 5:39 PM
 */
@NoRepositoryBean
public interface WrapperJpaRepository<T, ID> extends JpaRepository<T, ID> {

    default T findOne(ID id) {
        return getOne(id);
    }

}
