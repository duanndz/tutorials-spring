package com.duanndz.data.jpa.command;

import com.duanndz.data.jpa.entities.Customer;
import com.duanndz.data.jpa.repositories.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created By duan.nguyen at 12/22/20 5:26 PM
 */
@Component
@Slf4j
public class DataCommandLineRunner implements CommandLineRunner {

    private final CustomerRepository customerRepository;

    @Autowired
    public DataCommandLineRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public void run(String... args) throws Exception {
//        long count = customerRepository.count();
//        Customer customer = new Customer("Duan-" + (count + 1), "Nguyen");
//        customerRepository.save(customer);

        Customer queryCustomer = customerRepository.findOne(4L);
        log.info(queryCustomer.toString());
    }
}
