package com.duanndz.data.jpa.repositories;

import com.duanndz.data.jpa.entities.Customer;
import org.springframework.stereotype.Repository;

/**
 * Created By duan.nguyen at 12/22/20 5:27 PM
 */
@Repository
public interface CustomerRepository extends WrapperJpaRepository<Customer, Long> {

}
