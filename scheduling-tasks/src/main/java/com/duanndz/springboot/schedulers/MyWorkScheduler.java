package com.duanndz.springboot.schedulers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Created By duan.nguyen at 7/14/20 2:12 PM
 */
@Slf4j
@Component
public class MyWorkScheduler {

    @Scheduled(cron = "${scheduler.work.wakeup.at}")
    public void wakeup() {
        log.info("I wakeup at {}", LocalDateTime.now().toString());
    }

    // say hello each 5 min.
    @Scheduled(initialDelay = 30000, fixedRate = 300000)
    public void sayHello() {
        log.info("Say hello at {}", LocalDateTime.now().toString());
    }

}
