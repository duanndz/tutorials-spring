package com.duanndz.springboot.tasks;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created By duan.nguyen at 7/14/20 2:14 PM
 */
@Configuration
public class MyTaskExecutor {

    // @Bean
    public TaskExecutor taskExecutor() {
        // corePoolSize: 5, maxPoolSize: 20, keep-alive: 60s, queue-capacity: 1000
        Executor executor = new ThreadPoolExecutor(5, 20, 60,
            TimeUnit.SECONDS, new LinkedBlockingQueue<>(1000));
        return new ConcurrentTaskExecutor(executor);
    }

}
