package com.duanndz.springboot.tasks;

import lombok.extern.slf4j.Slf4j;

/**
 * Created By duan.nguyen at 7/14/20 11:39 AM
 */
@Slf4j
public class MessagePrinterTask implements Runnable {

    private final String message;

    public MessagePrinterTask(String message) {
        this.message = message;
    }

    @Override
    public void run() {
        try {
            log.info("Thread: {}, msg: {}", Thread.currentThread().getName(), message);
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
    }

}
