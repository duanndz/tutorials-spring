package com.duanndz.springboot.tasks;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Created By duan.nguyen at 7/14/20 2:42 PM
 */
@Slf4j
@Component
public class MyTaskExecution {

    @Async
    public void wakeup() {
        try {
            log.info("Thread: {} wakeup running.", Thread.currentThread().getName());
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
