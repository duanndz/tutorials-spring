package com.duanndz.springboot;

import com.duanndz.springboot.tasks.MyTaskExecution;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * Created By duan.nguyen at 7/14/20 2:41 PM
 */
@Slf4j
@Component
public class MyTaskRunner implements ApplicationRunner {

    private final MyTaskExecution myTaskExecution;

    public MyTaskRunner(MyTaskExecution myTaskExecution) {
        this.myTaskExecution = myTaskExecution;
    }

    @Override
    public void run(ApplicationArguments args) {
        for (int i = 0; i < 25; i++) {
            myTaskExecution.wakeup();
        }
    }
}
