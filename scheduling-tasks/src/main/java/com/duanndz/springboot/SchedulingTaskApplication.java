package com.duanndz.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created By duan.nguyen at 7/14/20 10:38 AM
 */
@EnableAsync
@EnableScheduling
@SpringBootApplication
public class SchedulingTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(SchedulingTaskApplication.class, args);
    }

}
