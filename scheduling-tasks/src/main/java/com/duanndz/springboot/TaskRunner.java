package com.duanndz.springboot;

import com.duanndz.springboot.tasks.MessagePrinterTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

/**
 * Created By duan.nguyen at 7/14/20 1:04 PM
 */
@Slf4j
@Component
public class TaskRunner implements ApplicationRunner {

    private final TaskExecutor taskExecutor;

    public TaskRunner(@Qualifier("taskExecutor") TaskExecutor taskExecutor) {
        log.info("Task Executor class: {}", taskExecutor.getClass().getName());
        this.taskExecutor = taskExecutor;
    }

    @Override
    public void run(ApplicationArguments args) {
        for (int i = 0; i < 25; i++) {
            String message = String.format("Message %d", (i + 1));
            taskExecutor.execute(new MessagePrinterTask(message));
        }
    }
}
