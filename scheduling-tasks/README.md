# Task Execution and Scheduling

## The Spring TaskExecutor Abstraction

    Executors are the JDK name for the concept of thread pools. 
    Spring’s TaskExecutor interface is identical to the java.util.concurrent.Executor interface.
    The TaskExecutor was originally created to give other Spring components an abstraction for thread pooling where needed.
    
1. TaskExecutor Types
    
    Spring includes a number of pre-built implementations of TaskExecutor.

        - SyncTaskExecutor
        - SimpleAsyncTaskExecutor
        - ConcurrentTaskExecutor: 
            we recommend that you use a ConcurrentTaskExecutor instead.
        - ThreadPoolTaskExecutor
        - WorkManagerTaskExecutor
        - DefaultManagedTaskExecutor
        
## The Spring TaskScheduler Abstraction

    Spring 3.0 introduced a TaskScheduler with a variety of methods for scheduling tasks to run at some point in the future.
    interface TaskScheduler 
    
1. Annotation Support for Scheduling and Asynchronous Execution
    
    To enable support for @Scheduled and @Async annotations, you can add @EnableScheduling and @EnableAsync to one of your @Configuration classes
        
        - 
        - 
