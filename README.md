# Spring Boot Reference Documentation
    Some examples about spring boot tutorials.

## 1. Spring Boot Documentation
    This section provides a brief overview of Spring Boot reference documentation. 
    It serves as a map for the rest of the document.

1.6. Learning about Spring Boot Features

- Core Features: SpringApplication | External Configuration | Profiles | Logging
- Web Applications: MVC | Embedded Containers
- Working with data: SQL | NO-SQL
- Messaging: Overview | JMS
- Testing: Overview | Boot Applications | Utils
- Extending: Auto-configuration | @Conditions

1.7. Moving to Production

- Management endpoints: Overview
- Connection options: HTTP | JMX
- Monitoring: Metrics | Auditing | HTTP Tracing | Process

1.8. Advanced Topics

- Spring Boot Applications Deployment: Cloud Deployment | OS Service
- Build tool plugins: Maven | Gradle
- Appendix: Application Properties | Configuration Metadata | Auto-configuration Classes | Test Auto-configuration Annotations | Executable Jars | Dependency Versions

## 2. Getting Started

2.1. Introducing Spring Boot


Spring Boot helps you to create stand-alone, production-grade Spring-based Applications that you can run.
We take an opinionated view of the Spring platform and third-party libraries, 
so that you can get started with minimum fuss. 
Most Spring Boot applications need very little Spring configuration.

- Our primary goals are:
    
    - Provide a radically faster and widely accessible getting-started experience for all Spring development.
    - Be opinionated out of the box but get out of the way quickly as requirements start to diverge from the defaults.
    - Provide a range of non-functional features that are common to large classes of projects 
        (such as embedded servers, security, metrics, health checks, and externalized configuration).
    - Absolutely no code generation and no requirement for XML configuration.

2.2.1. Servlet Containers
    
    - Tomcat 9.0
    - Jetty 9.4
    - Undertow 2.0

## 3. Using Spring Boot
    This section goes into more detail about how you should use Spring Boot. 
    It covers topics such as build systems, auto-configuration, and how to run your applications. 
    We also cover some Spring Boot best practices. 
    Although there is nothing particularly special about Spring Boot (it is just another library that you can consume), 
    there are a few recommendations that, when followed, make your development process a little easier.

3.1. Build Systems

3.1.5. Starters

    Starters are a set of convenient dependency descriptors that you can include in your application. 
    You get a one-stop shop for all the Spring and related technologies that you need without having 
    to hunt through sample code and copy-paste loads of dependency descriptors. 
    For example, if you want to get started using Spring and JPA for database access, 
    include the spring-boot-starter-data-jpa dependency in your project.

-   Table 1. Spring Boot application starters

        spring-boot-starter
        spring-boot-starter-data-jpa
        spring-boot-starter-hateoas
        spring-boot-starter-mail
        spring-boot-starter-quartz
        spring-boot-starter-security
        spring-boot-starter-web 

-   Table 2. Spring Boot production starters
    
        spring-boot-starter-actuator
    
-   Table 3. Spring Boot technical starters
        
        spring-boot-starter-undertow

3.3. Configuration Classes

    Spring Boot favors Java-based configuration. 
    Although it is possible to use SpringApplication with XML sources, 
        we generally recommend that your primary source be a single @Configuration class. 
        Usually the class that defines the main method is a good candidate as the primary @Configuration.

3.4. Auto-configuration

    Spring Boot auto-configuration attempts to automatically configure your Spring application 
        based on the jar dependencies that you have added.
    For example, if HSQLDB is on your classpath, and you have not manually configured any database connection beans, 
        then Spring Boot auto-configures an in-memory database.
    You need to opt-in to auto-configuration by adding the @EnableAutoConfiguration or 
        @SpringBootApplication annotations to one of your @Configuration classes.

3.5. Spring Beans and Dependency Injection
    
    You are free to use any of the standard Spring Framework techniques to define your beans 
        and their injected dependencies. 
    We often find that using @ComponentScan (to find your beans) and using @Autowired (to do constructor injection) works well.

3.6. Using the @SpringBootApplication Annotation
    
    Many Spring Boot developers like their apps to use auto-configuration, 
        component scan and be able to define extra configuration on their "application class".
    A single @SpringBootApplication annotation can be used to enable those three features, that is:

3.8. Developer Tools
    
    Developer tools are automatically disabled when running a fully packaged application. 
    If your application is launched from java -jar or if it is started from a special classloader, 
        then it is considered a “production application”.
    You can control this behavior by using the spring.devtools.restart.enabled system property. 
    To enable devtools, irrespective of the classloader used to launch your application, 
        set the -Dspring.devtools.restart.enabled=true system property. 
    This must not be done in a production environment where running devtools is a security risk. 
    To disable devtools, exclude the dependency or set the -Dspring.devtools.restart.enabled=false system property.

3.8.1. Property Defaults
    
    Several of the libraries supported by Spring Boot use caches to improve performance. 
    For example, template engines cache compiled templates to avoid repeatedly parsing template files. 
    Also, Spring MVC can add HTTP caching headers to responses when serving static resources.

    While caching is very beneficial in production, it can be counter-productive during development, 
        preventing you from seeing the changes you just made in your application. 
    For this reason, spring-boot-devtools disables the caching options by default.

3.8.2. Automatic Restart

3.8.3. LiveReload

## 4. Spring Boot Features
### 4.1. SpringApplication

- Spring Application example [spring-application](./spring-application/spring-application.md)
    
### 4.2. Externalized Configuration

### 4.3. Profiles

### 4.11. Working with SQL Databases



    









   

    













